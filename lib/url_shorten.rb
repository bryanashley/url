# frozen_string_literal: true

require "sinatra/base"
require "dotenv"
require "sequel"
require "hashids"

require "url_shorten/app"
require "url_shorten/link"