# frozen_string_literal: true
module UrlShorten
  class Link
    attr_accessor :id, :full_url

    def initialize(id, full_url)
      @id = id
      @full_url = full_url
    end

    def self.create(full_url)
      id = DB[:links].insert(full_url: full_url)

      Link.new(id, full_url)
    end

    def self.findByShort(short)
      id = Hasher.decode(short)
      result = DB[:links].where(:id => id).first

      Link.new(result[:id], result[:full_url]) unless result.nil?
    end

    def redirect
      DB[:redirects].insert(link_id: id)

      full_url
    end

    def stats
      {
        "last_day" => redirects.where{ created_at > Time.now - 60*24 }.count,
        "last_week" => redirects.where{ created_at > Time.now - 60*24*7 }.count,
        "all_time" => redirects.count
      }
    end

    def short_url
      Hasher.encode(id)
    end

    def redirects
      DB[:redirects].where(link_id: id)
    end
  end
end