# frozen_string_literal: true
module UrlShorten
  class App < Sinatra::Base

    get "/:short_link" do 
      link = Link.findByShort(params['short_link'])
      
      if link.nil?
        status 404
      else
        redirect(link.redirect)
      end
    end

    get "/:short_link/stats" do 
      link = Link.findByShort(params['short_link'])

      if link.nil?
        status 404
      else
        content_type :json
        link.stats.to_json
      end
    end

    post "/links" do
      payload = JSON.parse(request.body.read)
      link = Link.create(payload["full_url"])

      return link.short_url
    end

  end
end