# frozen_string_literal: true
require 'dotenv/load'
require 'sequel'

DB = Sequel.connect("postgres://#{ENV['DB_USER']}:#{ENV['DB_PASSWORD']}@#{ENV['DB_HOST']}:#{ENV['DB_PORT']}/#{ENV['DB_DATABASE']}")

DB.create_table :links do
  primary_key :id
  String :full_url
end

DB.create_table :redirects do
  primary_key :id
  foreign_key :link_id, :links
  DateTime :created_at, default: Sequel::CURRENT_TIMESTAMP 

  index :created_at
end