# frozen_string_literal: true

$LOAD_PATH.push File.expand_path("../lib", __FILE__)

require 'dotenv/load'
require "url_shorten"

DB = Sequel.connect("postgres://#{ENV['DB_USER']}:#{ENV['DB_PASSWORD']}@#{ENV['DB_HOST']}:#{ENV['DB_PORT']}/#{ENV['DB_DATABASE']}")
Hasher = Hashids.new ENV["APP_KEY"]

run UrlShorten::App